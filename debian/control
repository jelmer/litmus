Source: litmus
Section: web
Priority: optional
Maintainer: Jelmer Vernooĳ <jelmer@debian.org>
Homepage: http://www.webdav.org/neon/litmus/
Standards-Version: 4.6.1
Build-Depends: libneon27-gnutls-dev, debhelper-compat (= 12)
Vcs-Git: https://salsa.debian.org/jelmer/litmus.git
Vcs-Browser: https://salsa.debian.org/jelmer/litmus
Rules-Requires-Root: no

Package: litmus
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: WebDAV server test suite
 A WebDAV server protocol compliance test suite. Tests include:
 .
  * OPTIONS for DAV: header
  * PUT, GET with byte comparison
  * MKCOL
  * DELETE (collections, non-collections)
  * COPY, MOVE using combinations of:
   - overwrite t/f
   - destination exists/doesn't exist
   - collection/non-collection
  * Property manipulation and querying:
   - set, delete, replace properties
   - persist dead props across COPY
   - namespace handling
  * Locking
   - attempts to modify locked resource (as lock owner, not owner)
   - shared/exclusive locks, lock discovery
